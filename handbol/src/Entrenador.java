import java.util.ArrayList;
import java.util.List;

public class Entrenador implements Subject {
    private String _nom;
    private int _edat;
    private List<Observer> _observadors;

    public Entrenador(String nom, int edat) {
        _nom = nom;
        _edat = edat;
        _observadors = new ArrayList<>();
    }

    public void registrarObservadors(Observer o) {
        _observadors.add(o);
    }

    public void eliminarObservadors(Observer o) {
        _observadors.remove(o);
    }

    public void notificarObservadors(String info) {
        NotificacioEntrenador notificacioEntrenador = new NotificacioEntrenador(info);
        for (Observer o : _observadors) {
            notificacioEntrenador.afegirJugador(o);
            o.rebreNotificacio(notificacioEntrenador);
        }
    }
}
