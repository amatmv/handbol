import java.util.ArrayList;
import java.util.List;

public class NotificacioEntrenador implements Notificacio {
    private List<Jugador> _jugadors_desti;
    private String _notificacio;

    public NotificacioEntrenador(String info){
        _notificacio = info;
        _jugadors_desti = new ArrayList<Jugador>();
    }

    public void afegirJugador(Observer o){
        _jugadors_desti.add(o.jugador());
    }

    public void mostra(){
        System.out.print(_notificacio);
    }
}
