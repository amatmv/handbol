import java.util.Vector;

public class Rol {
    private String _nom;
    private String _tipus;
    private Vector<Preferencia> _preferencies;

    public Rol(String nom, String tipus) {
        _nom = nom;
        _tipus = tipus;
        _preferencies = new Vector<>();
    }

    public String nom() {
        return _nom;
    }

    public String tipus() {
        return _tipus;
    }

    public void establirPreferencia(Preferencia p) {
        _preferencies.add(p);
    }

    public Vector<Preferencia> preferencies() {
        return _preferencies;
    }

    @Override
    public String toString() {
        return nom() + ", " + tipus();
    }
}
