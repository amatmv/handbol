public class Pista {
    private String _nom;
    private String _ciutat;

    public Pista(String nomPista, String ciutat) {
        _nom = nomPista;
        _ciutat = ciutat;
    }
}

/*
* <Equip>
        <nom>Barça B Lassa</nom>
        <entrenador>Sergi Catarain Ruiz</entrenador>
        <localitat>Barcelona</localitat>
    </Equip>

    <Entrenador>
        <nom>Sergi Catarain Ruiz</nom>
        <edat>38</edat>
    </Entrenador>
    <Entrenador>
        <nom>Roi Sanchez Santabarbara</nom>
        <edat>34</edat>
    </Entrenador>

    <Pista>
        <nom>Pavelló Blanc I Verd</nom>
        <ciutat>Bordils</ciutat>
    </Pista>
    <Pista>
        <nom>Ciutat Esportiva Joan Gamper Pista Handbol</nom>
        <ciutat>Barcelona</ciutat>
    </Pista>

    <!--//////////////////////-->
    <!-- Jugadors del Bordils -->
    <!--//////////////////////-->
    <Jugador>
        <nom>Arnau Palahi Daranas</nom>
        <edat>22</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>David Maso Garolera</nom>
        <edat>29</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>David Masmiquel Vilanova</nom>
        <edat>26</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Esteve Ferrer Llobet</nom>
        <edat>29</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Francesc Reixach Prat</nom>
        <edat>28</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Gerard Farrarons Ribas</nom>
        <edat>25</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Ignacio Moreno Zazo</nom>
        <edat>30</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Jairo Montes Aristizabal</nom>
        <edat>22</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Joan Comas Calvet</nom>
        <edat>29</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Joan Vilanova Costa</nom>
        <edat>31</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Jordi Gonzalez Perez</nom>
        <edat>30</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>


    <!--////////////////////-->
    <!-- Jugadors del Barça -->
    <!--////////////////////-->

    <Jugador>
        <nom>Adria Leon Morales</nom>
        <edat>21</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Aleix Gomez Abello</nom>
        <edat>21</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Alex Pascual Garcia</nom>
        <edat>18</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>David Roca, Rodriguez</nom>
        <edat>18</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>David Estepa Ruiz</nom>
        <edat>19</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Dika Akwa Mem</nom>
        <edat>21</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Eduardo Calle Redondo</nom>
        <edat>18</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Gerard Forns Galve</nom>
        <edat>24</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Iosif-andrei Buzle</nom>
        <edat>19</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Jannek Klein</nom>
        <edat>19</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>
    <Jugador>
        <nom>Joaquim Vaillo Sole</nom>
        <edat>22</edat>
        <ofensiu>50</ofensiu>
        <defensiu>50</defensiu>
    </Jugador>

    <!-- Tipus de sancions -->
    <TipusSancio>
        <nom>Amonestació</nom>
    </TipusSancio>
    <TipusSancio>
        <nom>Exclusió</nom>
    </TipusSancio>
    <TipusSancio>
        <nom>Expulsió</nom>
    </TipusSancio>
* */
