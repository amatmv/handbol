import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Handbol {
    private static void entrenadorEnviaMissatgeJugadorsPista(BufferedReader inputReader, Partit partit, BaseDades bd) {
        int equip = -1;
        Scanner sc;
        while (!(between(equip, 1, 2))) {
            System.out.println("Escull entrenador:");
            System.out.println("\t1 - Entrenador equip local.");
            System.out.println("\t2 - Entrenador equip visitant.");
            try {
                equip = Integer.parseInt(inputReader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.print("Informacio a notificar: ");
        String info = null;
        try {
            info = inputReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (equip == 1)
            partit.equipLocal().entrenador().notificarObservadors(info);
        else if (equip == 2)
            partit.equipVisitant().entrenador().notificarObservadors(info);
    }

    private static void arbitAmonestaTarjaGrogaJugador(BufferedReader inputReader, Partit partit, BaseDades bd) {
        System.out.println("Llista de jugadors que estan jugant: ");
        int i = 1;
        Vector<EstatJugador> jugadorsEnPista = partit.jugadorsJugant();
        for (EstatJugador estatJugador: jugadorsEnPista) {
            System.out.println(i++ + " -> " + estatJugador.jugador());
        }

        int idJugador = -1;
        EstatJugador estatJugador = null;
        while (!(between(idJugador, 1, i))) {
            System.out.print("\nIntrodueix ID del jugador a amonestar: ");
            try {
                idJugador = Integer.parseInt(inputReader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                estatJugador = jugadorsEnPista.get(idJugador-1);
            } catch (IndexOutOfBoundsException ignored) {}
        }

        System.out.println("Arbitres del partit: ");
        Arbitre arbitreEscollit = null;
        i = 1;
        for (Arbitre a : partit.arbitres()) {
            System.out.println(i++ + " -> " + a);
        }

        int idArbitre = -1;
        while (!(between(idArbitre, 1, i))) {
            System.out.print("\nIntrodueix ID l'arbitre que amonesta: ");
            try {
                idArbitre = Integer.parseInt(inputReader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                arbitreEscollit = partit.arbitres().get(idArbitre-1);
            } catch (IndexOutOfBoundsException ignored) {}
        }

        // Comptem les amonestacions
        int comptadorAmonestacions = 0;
        for (Sancio sancio : estatJugador.obtenirSancions()) {
            if(sancio.tipus().equals("Amonestació")){
                comptadorAmonestacions++;
            }
        }

        TipusSancio tipusSancio;
        switch (comptadorAmonestacions) {
            case 0:
                tipusSancio = (TipusSancio) bd.buscarElement("nom",
                        "==", "Amonestació", "TipusSancio").get(0);
                break;
            case 1:
                tipusSancio = (TipusSancio) bd.buscarElement("nom",
                        "==", "Exclusió", "TipusSancio").get(0);
                break;
            default:
                tipusSancio = (TipusSancio) bd.buscarElement("nom",
                        "==", "Expulsió", "TipusSancio").get(0);
                break;
        }

        assert tipusSancio != null;

        Sancio sancio = new Sancio(tipusSancio, estatJugador.jugador());
        arbitreEscollit.notificarObservadors(sancio);
        System.out.println("S'ha sancionat el jugador: " + sancio.jugadorSancionat().nom() +
                ", que ara acumula " + estatJugador.obtenirSancions().size() + " sancions.");
    }

    private static void jugadorAgafaRolAltreJugador(BufferedReader inputReader, Partit partit, BaseDades bd) {
        System.out.println("Llista de jugadors que estan jugant: ");
        int i = 1;
        Vector<EstatJugador> jugadorsEnPista = partit.jugadorsJugant();
        for (EstatJugador estatJugador: jugadorsEnPista) {
            System.out.println(i++ + " -> " + estatJugador.jugador() + ". Rol: " + estatJugador.rolActual());
        }

        int idJugador = -1;
        EstatJugador estatJugador = null;
        while (!(between(idJugador, 1, i))) {
            System.out.print("\nIntrodueix ID del jugador a canviar el rol: ");
            try {
                idJugador = Integer.parseInt(inputReader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                estatJugador = jugadorsEnPista.get(idJugador-1);
            } catch (IndexOutOfBoundsException ignored) {}
        }

        System.out.println("Rols a adoptar possibles");
        i = 1;
        Vector<Object> rols = bd.buscarElement(null, null, null, "Rol");
        for (Object rol: rols) {
            System.out.println(i++ + " -> " + rol);
        }

        int idRol = -1;
        Rol rolNou = null;
        while (!(between(idRol, 1, i))) {
            System.out.print("\nIntrodueix ID del rol a adoptar: ");
            try {
                idRol = Integer.parseInt(inputReader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                rolNou = (Rol) rols.get(idRol-1);
            } catch (IndexOutOfBoundsException ignored) {}
        }

        estatJugador.adoptarRol(rolNou);

        System.out.println("El jugador: " + estatJugador.jugador().nom() +
                " ha adoptat el rol: " + estatJugador.rolActual());
        System.out.println("Ara tinc noves preferències:");
        for (Preferencia pref : estatJugador.preferencies()) {
            System.out.println(pref);
        }
    }

    private static void inicialitzarJugadors(BaseDades bd, Vector<Object> jugadors,
                                             Equip equip, Vector<Arbitre> arbitres) {
        int jugadorsJugant = 0;
        // Farem sortir 6 jugadors del equip indicat a la pista (jugant = true) i
        // farem que observin a l'arbitre i l'entrenador
        Vector<Object> rols = bd.buscarElement(null, null, null, "Rol");
        // Anirem escollint rols aleatoris i els anirem assignant als jugadors que surtin a pista
        Collections.shuffle(rols);
        boolean porterAssignat = false;
        for (Object jug : jugadors) {
            Jugador j = (Jugador) jug;
            EstatJugador estatJugador = j.retornaEstat();
            if (jugadorsJugant++ < 7) {
                Rol rolAleatori = (Rol) rols.get(jugadorsJugant);
                porterAssignat = rolAleatori.nom().equals("Porter");
                // Si estem en l'ultim jugador de pista i no hem escollit porter encara, l'assignarem ara.
                if (!porterAssignat && jugadorsJugant == 7) {
                    Rol rolPorter = (Rol) bd.buscarElement("nom", "==",
                            "Porter", "Rol").get(0);
                    estatJugador.adoptarRol(rolPorter);
                } else {
                    estatJugador.adoptarRol(rolAleatori);
                }
                estatJugador.sortirALaPista();
                for (Arbitre a : arbitres)
                    a.registrarObservadors(estatJugador);
                equip.entrenador().registrarObservadors(estatJugador);
            }
            equip.afegirJugador(j);
        }
    }

    private static Partit iniciarPartit(BaseDades bd) {
        Pista pista = (Pista) bd.buscarElement("nom", "==", "Pavelló Blanc I Verd", "Pista").get(0);
        Date data = new Date();

        Partit partitVigent = new Partit(pista, data);
        Arbitre arbitre1 = new Arbitre("Arbitre Fictici", "nº Colegiat Fictici", "DNI fictici");
        partitVigent.afegirArbitre(arbitre1);
        Arbitre arbitre2 = new Arbitre("Arbitre Fictici 2", "nº Colegiat Fictici 2", "DNI fictici 2");
        partitVigent.afegirArbitre(arbitre2);

        Equip equipLocal = (Equip) bd.buscarElement("nom", "==", "Club Handbol Bordils", "Equip").get(0);
        Vector<Object> jugadors = bd.buscarElement("equip", "==", equipLocal, "Jugador");
        inicialitzarJugadors(bd, jugadors, equipLocal, partitVigent.arbitres());
        partitVigent.establirEquipLocal(equipLocal);

        Equip equipVisitant = (Equip) bd.buscarElement("nom", "==", "Barça B Lassa", "Equip").get(0);
        jugadors = bd.buscarElement("equip", "==", equipVisitant, "Jugador");
        inicialitzarJugadors(bd, jugadors, equipVisitant, partitVigent.arbitres());
        partitVigent.establirequipVisitant(equipVisitant);

        return partitVigent;
    }

    public static void main(String[] args) {

        BaseDades bd;
        try {
            bd = new BaseDades("dades_handbol.xml");
        } catch (DatabaseException dbExept) {
            dbExept.printStackTrace();
            return;
        }
        Partit partit = iniciarPartit(bd);

        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("=============================================================");
            System.out.println("Opcions:");
            System.out.println("1 - Com a entrenador: enviar missatge als jugadors de pista.");
            System.out.println("2 - Com a àrbit: amonestar amb tarjeta groga a un jugador.");
            System.out.println("3 - Com a jugador: canviar de rol.");
            System.out.println("0 - Sortir.");
            System.out.println("=============================================================");

            int opcio = -1;
            while (!(between(opcio, 0, 3))) {
                System.out.print("Quina opció vols? [0-3] ");
                try {
                    opcio = Integer.parseInt(sc.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("\n\n\n\n\n\n\n");
            switch (opcio) {
                case 1:
                    entrenadorEnviaMissatgeJugadorsPista(sc, partit, bd);
                    break;
                case 2:
                    arbitAmonestaTarjaGrogaJugador(sc, partit, bd);
                    break;
                case 3:
                    jugadorAgafaRolAltreJugador(sc, partit, bd);
                    break;
                case 0:
                    return;
                default:
                    break;
            }
        }
    }

    static public boolean between(int n, int left, int right) {
        return n >= left && n <= right;
    }
}
