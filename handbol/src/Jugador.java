public class Jugador {
    private String _nom;
    private Integer _edat;
    private EstatJugador _estat;
    private Equip _equip;

    public Jugador(String nom, Integer edat, Equip equip){
        _nom = nom;
        _edat = edat;
        _estat = new EstatJugador(this);
        _equip = equip;
    }

    public EstatJugador retornaEstat() {
        return _estat;
    }

    public String nom() {
        return _nom;
    }

    @Override
    public String toString() {
        return _nom + ". Equip: " + _equip.nom();
    }
}
