public class Sancio implements Notificacio {
    TipusSancio _tipus;
    Jugador _jugadorSancionat;

    public Sancio(TipusSancio tipus, Jugador jugadorSancionat) {
        _tipus = tipus;
        _jugadorSancionat = jugadorSancionat;
    }

    public String tipus() {
        return _tipus.toString();
    }

    public Jugador jugadorSancionat() {
        return _jugadorSancionat;
    }

    public void mostra(){};
}
