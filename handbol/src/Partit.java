import java.util.Date;
import java.util.Vector;

public class Partit {
    private Date _data;
    private Equip _local;
    private Equip _visitant;
    private int _golsLocal;
    private int _golsVisitant;
    private Pista _pista;
    private Vector<Arbitre> _arbitres;

    public Partit(Pista pista, Date data) {
        _data = data;
        _pista = pista;
        _golsLocal = _golsVisitant = 0;
        _arbitres = new Vector<>(2);
        Vector<Arbitre> res = new Vector<>(2);
    }

    public Equip equipLocal() {
        return _local;
    }

    public Equip equipVisitant() {
        return _visitant;
    }

    public Vector<Arbitre> arbitres() {
        return _arbitres;
    }

    public Vector<EstatJugador> jugadorsJugant() {
        Vector<EstatJugador> res = new Vector<>();
        for (Jugador jug : _local.jugadors()) {
            if (jug.retornaEstat().estaJugant())
                res.add(jug.retornaEstat());
        }
        for (Jugador jug : _visitant.jugadors()) {
            if (jug.retornaEstat().estaJugant())
                res.add(jug.retornaEstat());
        }
        return res;
    }

    public Vector<EstatJugador> jugadorsBanca() {
        Vector<EstatJugador> res = new Vector<>();
        for (Jugador jug : _local.jugadors()) {
            if (!jug.retornaEstat().estaJugant())
                res.add(jug.retornaEstat());
        }
        for (Jugador jug : _visitant.jugadors()) {
            if (!jug.retornaEstat().estaJugant())
                res.add(jug.retornaEstat());
        }
        return res;
    }

    public void afegirArbitre(Arbitre arbitre) {
        if (_arbitres.size() <= 2)
            _arbitres.add(arbitre);
    }

    public void establirEquipLocal(Equip equipLocal) {
        _local = equipLocal;
    }

    public void establirequipVisitant(Equip equipVisitant) {
        _visitant = equipVisitant;
    }
}
