public interface Observer {
    void rebreNotificacio(Notificacio notificacio);
    Jugador jugador();
}
