public class Preferencia {
    private String _nom;

    public Preferencia(String nom) {
        _nom = nom;
    }

    @Override
    public String toString() {
        return _nom;
    }
}
