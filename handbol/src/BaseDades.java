import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import org.w3c.dom.Node;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

class BaseDades {
    private List<String> _llistaTaules;
    private Map<String, Map<Integer, Object>> _esquema;

    BaseDades(String fitxerXML) throws DatabaseException {
        _llistaTaules = new LinkedList<>();

        // Ordenat per ordre jeràrquic de dades
        _llistaTaules.add("Entrenador");
        _llistaTaules.add("Equip");
        _llistaTaules.add("Jugador");
        _llistaTaules.add("TipusSancio");
        _llistaTaules.add("Pista");
        _llistaTaules.add("Preferencia");
        _llistaTaules.add("Rol");
        _esquema = new HashMap<>();
        for (String taula : _llistaTaules) {
            _esquema.put(taula, new HashMap<>(0));
        }
        try {
            carregarDades(fitxerXML);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("Base de dades: No s'han pogut llegir les dades des del " +
                    "fitxer: \"" + fitxerXML + "\".");
        }
    }

    private void carregarDades(String fitxerXML) throws IOException, ParserConfigurationException, SAXException {
        File file = new File(fitxerXML);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        // Per a cada taula, carreguem les seves files
        Object elementACrear;
        for (String nomTaula : _llistaTaules) {
            NodeList nList = doc.getElementsByTagName(nomTaula);

            if (nList.getLength() == 0)
                continue;
            HashMap<Integer, Object> taula = new HashMap<>(nList.getLength());

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    elementACrear = crearElementDesDeNomTaula(nomTaula, eElement);
                    taula.put(temp+1, elementACrear);
                }
            }
            _esquema.put(nomTaula, taula);
        }
    }

    private Object crearElementDesDeNomTaula(String nomTaula, Element eElement) throws IOException {
        if (nomTaula.equals("Jugador")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            Integer edat = obtenirValorEnterEtiqueta("edat", eElement);
            String nomEquip = obtenirValorEtiqueta("equip", eElement);
            Equip equip = (Equip) buscarElement("nom", "==", nomEquip, "Equip").get(0);
            if (equip == null) {
                throw new IOException("El jugador: " + nom + " té definit un equip que no existeix.");
            }
            return new Jugador(nom, edat, equip);
        }
        if (nomTaula.equals("TipusSancio")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            return new TipusSancio(nom);
        }
        if (nomTaula.equals("Equip")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            String nomEntrenador = obtenirValorEtiqueta("entrenador", eElement);
            Vector<Object> res = buscarElement("nom", "==", nomEntrenador, "Entrenador");
            if (res.isEmpty())
                throw new IOException("L'equip: " + nom + " té definit un entrenador que no existeix.");
            Entrenador entrenador = (Entrenador) res.get(0);
            String localitat = obtenirValorEtiqueta("localitat", eElement);
            return new Equip(nom, entrenador, localitat);
        }
        if (nomTaula.equals("Pista")) {
            String nomPista = obtenirValorEtiqueta("nom", eElement);
            String ciutat = obtenirValorEtiqueta("ciutat", eElement);
            return new Pista(nomPista, ciutat);
        }
        if (nomTaula.equals("Entrenador")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            int edat = obtenirValorEnterEtiqueta("edat", eElement);
            return new Entrenador(nom, edat);
        }
        if (nomTaula.equals("Rol")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            String tipus = obtenirValorEtiqueta("tipus", eElement);
            if (!tipus.equals("Atacant") && !tipus.equals("Defensor"))
                throw new IOException("El Rol " + tipus + " no existeix!");
            Rol r = new Rol(nom, tipus);
            NodeList preferencies = eElement.getElementsByTagName("preferencia");
            for (int i = 0; i < preferencies.getLength(); i++) {
                Vector<Object> res = buscarElement(
                        "nom", "==", preferencies.item(i).getTextContent(), "Preferencia");
                if (res.isEmpty())
                    throw new IOException("L'equip: " + nom + " té definit un entrenador que no existeix.");
                r.establirPreferencia((Preferencia) res.get(0));
            }
            return r;
        }
        if (nomTaula.equals("Preferencia")) {
            String nom = obtenirValorEtiqueta("nom", eElement);
            return new Preferencia(nom);
        }
        return null;
    }

    public String obtenirValorEtiqueta(String tag, Element eElement) {
        return eElement.getElementsByTagName(tag).item(0).getTextContent();
    }

    public int obtenirValorEnterEtiqueta(String tag, Element eElement) {
        return Integer.parseInt(eElement.getElementsByTagName(tag).item(0).getTextContent());
    }

    public Vector<Object> buscarElement(String nomCamp, String operador, Object valor, String nomTaula) {
        assert _llistaTaules.contains(nomTaula);
        boolean compararValors = ! (nomCamp == null && operador == null && valor == null);
        // En el cas que no tots siguin nuls, volem que no n'hi hagi cap de nul
        assert !compararValors || (nomCamp != null && operador != null && valor != null);
        Map<Integer, Object> taula = _esquema.get(nomTaula);

        Vector<Object> res = new Vector<>();
        if (taula.isEmpty())
            return res;

        for (Object obj : taula.values())
        {
            if (!compararValors || comparar(obj, nomCamp, operador, valor)){
                res.add(obj);
            }
        }
        return res;
    }

    private boolean comparar(Object obj, String camp, String operador, Object valor) {
        try {
            Field f = obj.getClass().getDeclaredField("_" + camp);
            f.setAccessible(true);
            Object it_tab = f.get(obj);
            if (operador.equals("=="))
                if (valor instanceof String)
                    return it_tab.equals(valor);
                else
                    return valor == it_tab;
            else if (operador.equals("!="))
                return ! it_tab.equals(valor);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {}

        return false;
    }
}
