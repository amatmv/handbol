public class TipusSancio{
    private String _nom;

    public TipusSancio(String nom) {
        _nom = nom;
    }

    public String toString() {
        return _nom;
    }
}
