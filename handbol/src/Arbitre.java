import java.util.ArrayList;
import java.util.List;

public class Arbitre implements Subject {
    private String _nom;
    private String _nColegiat;
    private String _dni;
    private List<Observer> _observadors;

    public Arbitre(String nom, String nColegiat, String dni) {
        _nom = nom;
        _nColegiat = nColegiat;
        _dni = dni;
        _observadors = new ArrayList<>();
    }

    public void registrarObservadors(Observer o) {
        _observadors.add(o);
    }

    public void eliminarObservadors(Observer o) {
        _observadors.remove(o);
    }

    public void notificarObservadors(Sancio sancio) {
        for (Observer observador : _observadors) {
            EstatJugador jugadorSancionat = sancio.jugadorSancionat().retornaEstat();

            if (observador == jugadorSancionat && jugadorSancionat.estaJugant()) {
                observador.rebreNotificacio(sancio);
            }
        }
    }

    @Override
    public String toString() {
        return _nom;
    }
}
