import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class EstatJugador implements Observer {
    private float _velocitat;
    private boolean _jugant;
    private List<Rol> _rols_adoptats;
    private List<Notificacio> _notificacions;
    private Jugador _jugador;
    private Vector<Preferencia> _preferencies;

    public EstatJugador(Jugador jugador) {
        _notificacions = new LinkedList<>();
        _rols_adoptats = new LinkedList<>();
        _jugador = jugador;
        _velocitat = 0;
        _jugant = false;
        _preferencies = new Vector<>();
    }

    public void adoptarRol(Rol nouRol) {
        _preferencies = nouRol.preferencies();
        _rols_adoptats.add(nouRol);
    }

    public List<Sancio> obtenirSancions() {
        List<Sancio> res = new LinkedList<>();
        for (Notificacio noti : _notificacions) {
            if (noti instanceof Sancio)
                res.add((Sancio) noti);
        }
        return res;
    }

    public boolean estaJugant() {
        return _jugant;
    }

    public void sortirALaPista() {
        _jugant = true;
    }

    public void sortirDeLaPista() {
        _jugant = false;
    }

    public Jugador jugador() {
        return _jugador;
    }

    public void rebreNotificacio(Notificacio notificacio) {
        _notificacions.add(notificacio);
    }

    public void darreraNotificacio(){
        _notificacions.get(_notificacions.size()-1).mostra();
    }

    public String rolActual() {
        if (_rols_adoptats.size() < 1)
            return null;
        Rol r = _rols_adoptats.get(_rols_adoptats.size() - 1);
        return r.nom() + ", " + r.tipus();
    }

    public Vector<Preferencia> preferencies() {
        return _preferencies;
    }
}
