import java.util.Vector;

public class Equip {
    private String _nom;
    private Entrenador _entrenador;
    private String _localitat;

    private Vector<Jugador> _jugadors;
    private Jugador _capita;

    public Equip(String nom, Entrenador entrenador, String localitat) {
        _nom = nom;
        _entrenador = entrenador;
        _localitat = localitat;
        _jugadors = new Vector<>();
    }

    public String nom() {
        return _nom;
    }

    public void establirCapita(Jugador capita) {
        _capita = capita;
    }

    public Entrenador entrenador() {
        return _entrenador;
    }

    public void afegirJugador(Jugador jugador) {
        _jugadors.add(jugador);
    }

    public Vector<Jugador> jugadors() {
        return _jugadors;
    }
}
